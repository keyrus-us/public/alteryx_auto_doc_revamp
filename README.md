# Alteryx Auto Documenter

Alteryx users continually need some form of documentation generator from workflows. Users
need this form of documentation generator for many reasons, ranging from convenience to
compliance. Keyrus has set out to make the process easier by automatically generating
documentation based on the content of a workflow.   
When we presented our original version of the Autodocumenter tool for Alteryx, feedback from
the community was positive but there were shortcomings. The key issues with the original
documentation utility were:  

• It did not support workflows with containers  
• It did not provide an order of tools (the document would not be helpful in rebuilding a
workflow)  
• It did not provide enough details about the configurations of various tools.  

The new workflow addresses these shortcomings. Additionally, the newest version generates a
large image of the full workflow as part of the documentation. If required, a larger version of this
image is saved to the Autodocumenter’s outputs directory).  



## Notes for May 19, 2020 Version

Every node is assigned a group based on 1. Container membership of top level containers and 2. dependency on top level containers.  
Dependency on container and membership in container is flagged by multipliying container id by -1 when generating the group.  
Ex:   
• If a node is in container with nodeId 20, the node is assigned to group (-20).  
• If a node is not in container 20, but has an upstream dependency only on container 20, it will be assigned group (20).  
• If a node is not in container 20, but depends on container 20 and 40, it will be assigned group (20,40).  
• If a node does not depend on any containers, it will be assigned group (0).  
Finally, a node may have dependencies on non-container and container nodes- in which case it will be assigned a group like (0,20,40).  

Groups are then sorted by the execution order of the earliest-running node in the container.   
This does not guarantee that all of the container's dependencies have completed running by the time the container is documented (consider the case where there is an input with no dependencies in a container, but the next step of the container joins this input with the result of prior containers, etc...).  

## Notes for September 28, 2020 Version

Container NONADMIN versions may not be used without elevating to admin privledges to install.  In addition, an Installer for the required packages is now included. When running the installer, you will be prompted for the folder containing the package files- you must select the packages folder zipped together with the installer.  
The installer will work for nonadmin installs only for the September 28, 2020 and later versions.